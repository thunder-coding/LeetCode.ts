import Solution from './0002'

test('LeetCode Problem #2', () => {
    expect(Solution([1, 2, 3], [6, 8, 3])).toStrictEqual([7, 0, 7])
    expect(Solution([2, 4, 3], [5, 6, 4])).toStrictEqual([7, 0, 8])
})

/**
 * Solution to LeetCode [Problem #4](https://leetcode.com/problems/median-of-two-sorted-arrays/)
 */
export default function Solution(array1: number[], array2: number[]) {
    let mergedArray = [...array1, ...array2]
    mergedArray.sort((a, b) => a - b)
    if (mergedArray.length % 2)
        return mergedArray[mergedArray.length / 2 - 0.5] as number
    return (
        (((mergedArray[mergedArray.length / 2] as number) +
            // @ts-ignore
            mergedArray[mergedArray.length / 2 - 1]) as number) / 2
    )
}

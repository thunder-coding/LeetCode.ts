/**
 * Solution to LeetCode [Problem #3](https://leetcode.com/problems/longest-substring-without-repeating-characters/)
 */
export default function Solution(s: string): number {
    let subString = ''
    let largestStringLength = 0
    for (let index = 0; index < s.length; index++) {
        subString = s[index] as string
        for (;;) {
            let y = s[subString.length + index] as string
            if (!subString.includes(y) && y != undefined) subString += y
            else break
        }
        if (subString.length > largestStringLength)
            largestStringLength = subString.length
    }
    return largestStringLength
}

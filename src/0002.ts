/**
 * Solution to LeetCode [Problem #2](https://leetcode.com/problems/add-two-numbers/)
 */
export default function Solution(
    List1: Array<number>,
    List2: Array<number>
): Array<number> {
    let revList1 = new Array(...List1.values()).reverse()

    let revList2 = new Array(...List2.values()).reverse()
    let revNumber1 = revList1.join('')
    let revNumber2 = revList2.join('')
    let revSum = +revNumber1 + +revNumber2
    return revSum
        .toString()
        .split('')
        .reverse()
        .map((x) => +x)
}

import Solution from './0001'

test('LeetCode Problem #1', () => {
    expect(
        Solution(
            [28, 68, 98, 67, 12, 6, 53, 45, 82, 95, 75, 85, 32, 69, 74],
            153
        )
    ).toStrictEqual([1, 11])
})

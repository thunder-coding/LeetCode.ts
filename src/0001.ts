/**
 * Solution to LeetCode [Problem #1](https://leetcode.com/problems/two-sum/description/)
 */
export default function Solution(
    numbers: Array<number>,
    target: number
): [number, number] {
    let returnable: [number, number] = [0, 0]
    numbers.some((num, index) => {
        let x = numbers.indexOf(target - num, index + 1)
        if (x != -1) {
            returnable = [index, x]
            return true
        }
        return false
    })
    return returnable
}

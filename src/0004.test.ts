import Solution from './0004'

test('LeetCode Problem #4', () => {
    expect(Solution([1, 2, 3], [4, 7, 9])).toEqual(3.5)
    expect(Solution([], [1])).toEqual(1)
    expect(Solution([2, 3, 5], [1])).toEqual(2.5)
    expect(Solution([8], [2])).toEqual(5)
})

import Solution from './0003'

test('LeetCode Problem #3', () => {
    expect(Solution('abcabc')).toEqual(3)
    expect(Solution('abcdeff')).toEqual(6)
    expect(Solution('xxxxx')).toEqual(1)
    expect(Solution('pwwkew')).toEqual(3)
})
